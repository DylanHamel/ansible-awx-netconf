module ericsson-vpls-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-vpls-ipos";

  prefix "vplsipos";

  import ietf-yang-types {
    prefix "yang";
  }

  import ericsson-contexts-ipos {
    prefix "ctxsipos";
  }

  import ericsson-vpws-ipos {
    prefix "vpwsipos";
  }

  import ericsson-bridge-ipos {
    prefix "bridgeipos";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-vpls-ipos
     Copyright (c) 2017 Ericsson AB.
     All rights reserved";

  revision "2017-03-29" {
    description
      "initial revision";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  grouping vpls-grp {
    description
      "VPLS configuration mode";
    /*
    pseudowire { vlan ( vlan-val ) | { instance (instance-id) [
     spoke ] } }
    */
    container pseudowire {
      description
        "Configure pseudowire";
      leaf vlan {
        type string {
          pattern '(untagged|\d{1,4}|\d{1,4}\-\d{1,4})(,\d{1,4}|'
          + ',\d{1,4}\-\d{1,4}){0,4}';
        }
        description
          "Configure";
      }
      list instance {
        key "instance-id";
        description
          "Set PW instance id";
        leaf instance-id {
          type leafref {
            path "/ctxsipos:contexts/vpwsipos:pseudowire/vpwsipo"
            + "s:instance/vpwsipos:instance";
          }
          description
            "PW Instance ID";
        }
        leaf spoke {
          type empty;
          description
            "Configure this PW as spoke";
        }
        uses pw-instance-grp;
      }
    }
  }

  grouping pw-instance-grp {
    description
      "VPLS PW instance configuration mode";
    /*
    split-horizon-group (shg-name)
    */
    list split-horizon-group {
      key "split-horizon-group";
      description
        "Add pseudowire to split horizon group";
      leaf split-horizon-group {
        type string;
        description
          "Split horizon group name";
      }
    }
    /*
    profile (profile-name)
    */
    leaf profile {
      type leafref {
        path "/bridgeipos:bridge/bridgeipos:profile/bridgeipos:p"
        + "rofile";
      }
      description
        "Configure VPLS profile";
    }
    /*
    mac-entry static < vlan (vlan-id) > < mac (mac-val) >
    */
    list mac-entry-static {
      key "mac vlan";
      description
        "entry";
      leaf mac {
        type yang:mac-address;
        description
          "Mac";
      }
      leaf vlan {
        type uint16 {
          range "1..4095";
        }
        description
          "VLAN-ID, vlan-id for 4095 is invalid.";
      }
    }
    /*
    neighbor mac-flush
    */
    leaf neighbor-mac-flush {
      type empty;
      description
        "Send MAC flush message at the hub node";
    }
    /*
    native vlan (vlan-val)
    */
    container native {
      description
        "Configure native";
      leaf vlan {
        type uint16 {
          range "1..4094";
        }
        description
          "Configure native";
      }
    }
  }

  augment "/bridgeipos:bridge/bridgeipos:bridge-instance" {
    description
      "ericsson-vpls";
    /*
    vpls
    */
    container vpls {
      presence "";
      description
        "Configure VPLS attributes";
      uses vpls-grp;
    }
  }

}
