module ericsson-ipsec-router6000 {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-ipsec-router6000";

  prefix "ipsecrouter6000";

  import ietf-yang-types {
    prefix "yang";
  }

  import ietf-inet-types {
    prefix "inet";
  }

  import ericsson-contexts-router6000 {
    prefix "ctxsr6k";
  }

  import ericsson-context-router6000 {
    prefix "ctxr6k";
  }

  import ericsson-tunnel-router6000 {
    prefix "tunnelrouter6000";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-ipsec-router6000
     Copyright (c) 2018 Ericsson AB.
     All rights reserved";

  revision "2018-10-16" {
    description
      "ipsec yang seq support compatibility";
    reference
      "rfc6020";
    yexte:version "2";
    yexte:release "1";
    yexte:correction "1";
  }

  revision "2018-09-29" {
    description
      "fix ike/ipsec policy seq issue";
    reference
      "rfc6020";
    yexte:version "2";
    yexte:release "1";
    yexte:correction "0";
  }

  revision "2018-09-27" {
    description
      "changing namespace for the model";
    reference
      "rfc6020";
    yexte:version "2";
    yexte:release "0";
    yexte:correction "0";
  }

  revision "2017-06-09" {
    description
      "initial revision";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  grouping ike-proposal-grp {
    description
      "IKE proposal mode";
    /*
    description (desc-str)
    */
    leaf description {
      type string {
        length "1..256";
      }
      description
        "Enter the proposal description text";
    }
    /*
    authentication algorithm { hmac-md5-96 | hmac-sha1-96 |
     hmac-sha2-256-128 | hmac-aes-xcbc }
    */
    container authentication-algorithm {
      description
        "Enter the authentication algorithm";
      choice authentication-algorithm {
        description
          "IPOS choice";
        case hmac-md5-96 {
          leaf hmac-md5-96 {
            type empty;
            description
              "Set the authentication algorithm to hmac-md5-96";
          }
        }
        case hmac-sha1-96 {
          leaf hmac-sha1-96 {
            type empty;
            description
              "Set the authentication algorithm to hmac-sha1-96";
          }
        }
        case hmac-sha2-256-128 {
          leaf hmac-sha2-256-128 {
            type empty;
            description
              "Set the authentication algorithm to
               hmac-sha2-256-128";
          }
        }
        case hmac-aes-xcbc {
          leaf hmac-aes-xcbc {
            type empty;
            description
              "Set the authentication algorithm to
               hmac-aes-xcbc";
          }
        }
      }
    }
    /*
    encryption algorithm { des-cbc | 3des-cbc | aes-128-cbc |
     aes-192-cbc | aes-256-cbc }
    */
    container encryption {
      description
        "Specify IKE proposal encryption configuration";
      leaf algorithm {
        type enumeration {
          enum des-cbc {
            value 0;
            description
              "Set the encryption algorithm to des-cbc";
          }
          enum 3des-cbc {
            value 1;
            description
              "Set the encryption algorithm to 3des-cbc";
          }
          enum aes-128-cbc {
            value 2;
            description
              "Set the encryption algorithm to aes-128-cbc";
          }
          enum aes-192-cbc {
            value 3;
            description
              "Set the encryption algorithm to aes-192-cbc";
          }
          enum aes-256-cbc {
            value 4;
            description
              "Set the encryption algorithm to aes-256-cbc";
          }
        }
        description
          "Enter the encryption algorithm";
      }
    }
    /*
    dh-group { 14 | 1 | 2 | 5 }
    */
    leaf dh-group {
      type enumeration {
        enum 14 {
          value 0;
          description
            "Set to Diffie-Hellman group 14 (MODP 2048-bit
             prime)";
        }
        enum 1 {
          value 1;
          description
            "Set to Diffie-Hellman group 1 (MODP 768-bit prime)";
        }
        enum 2 {
          value 2;
          description
            "Set to Diffie-Hellman group 2 (MODP 1024-bit
             prime)";
        }
        enum 5 {
          value 3;
          description
            "Set to Diffie-Hellman group 5 (MODP 1536-bit
             prime)";
        }
      }
      description
        "Specify Diffie-Hellman group for IKE key exchanges";
    }
    /*
    pseudo-random-function { hmac-md5 | hmac-sha1 | aes-128-xcbc
     | hmac-sha2-256 }
    */
    container pseudo-random-function {
      description
        "Specify Pseudo Ramdom Function for IKE key exchanges";
      choice pseudo-random-function {
        description
          "IPOS choice";
        case hmac-md5 {
          leaf hmac-md5 {
            type empty;
            description
              "Set to pseudo-random-function hmac-md5";
          }
        }
        case hmac-sha1 {
          leaf hmac-sha1 {
            type empty;
            description
              "Set to pseudo-random-function hmac-sha1";
          }
        }
        case aes-128-xcbc {
          leaf aes-128-xcbc {
            type empty;
            description
              "Set to pseudo-random-function aes-128-xcbc";
          }
        }
        case hmac-sha2-256 {
          leaf hmac-sha2-256 {
            type empty;
            description
              "Set to pseudo-random-function hmac-sha2-256";
          }
        }
      }
    }
  }

  grouping ipsec-proposal-grp {
    description
      "IPsec proposal mode";
    /*
    description (desc-str)
    */
    leaf description {
      type string {
        length "1..256";
      }
      description
        "Enter the proposal description text";
    }
    /*
    esp { encryption [ null | des-cbc | 3des-cbc | aes-128-cbc |
     aes-192-cbc | aes-256-cbc ] | authentication [ hmac-md5-96 |
     hmac-sha1-96 | hmac-sha2-256 ] }
    */
    container esp {
      description
        "Enter IPsec ESP configuration";
      container encryption {
        presence "";
        description
          "Enter ESP encryption configuration";
        leaf encryption-opt {
          type enumeration {
            enum null {
              value 0;
              description
                "set NULL encryption algorithm";
            }
            enum des-cbc {
              value 1;
              description
                "Set the encryption algorithm to des-cbc";
            }
            enum 3des-cbc {
              value 2;
              description
                "Set the encryption algorithm to 3des-cbc";
            }
            enum aes-128-cbc {
              value 3;
              description
                "Set the encryption algorithm to aes-128-cbc";
            }
            enum aes-192-cbc {
              value 4;
              description
                "Set the encryption algorithm to aes-192-cbc";
            }
            enum aes-256-cbc {
              value 5;
              description
                "Set the encryption algorithm to aes-256-cbc";
            }
          }
          description
            "leaf encryption-opt.";
        }
      }
      container authentication {
        presence "";
        description
          "Enter ESP authentication configuration";
        choice authentication-opt {
          description
            "IPOS choice";
          case hmac-md5-96 {
            leaf hmac-md5-96 {
              type empty;
              description
                "Set the authentication algorithm to
                 hmac-md5-96";
            }
          }
          case hmac-sha1-96 {
            leaf hmac-sha1-96 {
              type empty;
              description
                "Set the authentication algorithm to
                 hmac-sha1-96";
            }
          }
          case hmac-sha2-256 {
            leaf hmac-sha2-256 {
              type empty;
              description
                "Set the authentication algorithm to
                 hmac-sha2-256";
            }
          }
        }
      }
    }
  }

  grouping ipsec-policy-grp {
    description
      "IPsec policy mode";
    /*
    description (desc-str)
    */
    leaf description {
      type string {
        length "1..256";
      }
      description
        "Enter the policy description text";
    }
    /*
    perfect-forward-secrecy dh-group { 14 | 1 | 2 | 5 }
    */
    container perfect-forward-secrecy {
      description
        "Enter the PFS configuration";
      leaf dh-group {
        type enumeration {
          enum 14 {
            value 0;
            description
              "Set to Diffie-Hellman group 14 (MODP 2048-bit
               prime)";
          }
          enum 1 {
            value 1;
            description
              "Set to Diffie-Hellman group 1 (MODP 768-bit
               prime)";
          }
          enum 2 {
            value 2;
            description
              "Set to Diffie-Hellman group 2 (MODP 1024-bit
               prime)";
          }
          enum 5 {
            value 3;
            description
              "Set to Diffie-Hellman group 5 (MODP 1536-bit
               prime)";
          }
        }
        description
          "Specify Diffie-Hellman group for
           perfect-forward-secrecy";
      }
    }
    /*
    anti-replay-window { 0 | (sz-val) }
    */
    leaf anti-replay-window {
      type enumeration {
        enum 0 {
          value 0;
          description
            "Enter 0 to disable anti-replay-window";
        }
        enum sz-val {
          value 1;
          description
            "IPsec anti-replay-window size in multiple of 32";
        }
      }
      description
        "Enter the replay window size.";
    }
    /*
    lifetime { { seconds { 0 | (sec-val) } } | { kbytes (kb-val)
     } }
    */
    container lifetime {
      description
        "Configure lifetime for IPSEC SAs";
      leaf seconds {
        type enumeration {
          enum 0 {
            value 0;
            description
              "Enter lifetime 0 second for no timeout";
          }
          enum sec-val {
            value 1;
            description
              "IPSEC SA lifetime in seconds";
          }
        }
        description
          "Enter lifetime seconds for IPSEC SAs";
      }
      leaf kbytes {
        type uint32 {
          range "128..2147483647";
        }
        description
          "Enter lifetime kbytes for IPSEC SAs";
      }
    }
    /*
    seq (seq-num) proposal (proposal-name)
    */
    container seq {
      status obsolete;
      description
        "Specify IPSEC proposal sequence number";
      leaf seq {
        type uint8 {
          range "1..8";
        }
        mandatory true;
        description
          "Sequence number";
      }
      leaf proposal {
        type string;
        mandatory true;
        description
          "Configure IPSEC proposal for the policy";
      }
    }
    /*
    sequence (seq-num) proposal (proposal-name)
    */
    list sequence {
      key "sequence";
      description
        "Specify IPSEC proposal sequence number";
      leaf sequence {
        type uint8 {
          range "1..8";
        }
        description
          "Sequence number";
      }
      container proposal {
        description
          "Configure IPSEC proposal for the policy";
        leaf proposal {
          type string;
          mandatory true;
          description
            "Specify an IPSEC proposal name";
        }
      }
    }
  }

  grouping ike-policy-grp {
    description
      "IKE policy mode";
    /*
    description (desc-str)
    */
    leaf description {
      type string {
        length "1..256";
      }
      description
        "Enter the policy description text";
    }
    /*
    connection-type { initiator-only | responder-only | both }
    */
    container connection-type {
      description
        "Specify IKE connection type";
      choice connection-type {
        description
          "IPOS choice";
        case initiator-only {
          leaf initiator-only {
            type empty;
            description
              "Set IKE connection type initiator-only";
          }
        }
        case responder-only {
          leaf responder-only {
            type empty;
            description
              "Set IKE connection type responder-only";
          }
        }
        case both {
          leaf both {
            type empty;
            description
              "Set IKE connection type both initiator and
               responder";
          }
        }
      }
    }
    /*
    authentication { preshared-key | rsa-signature }
    */
    container authentication {
      description
        "specify the authentication method to be used in IKE";
      choice authentication {
        description
          "IPOS choice";
        case preshared-key {
          leaf preshared-key {
            type empty;
            description
              "use pre-shared key based authentication";
          }
        }
        case rsa-signature {
          leaf rsa-signature {
            type empty;
            description
              "use signature based authentication using PKI
               certificates";
          }
        }
      }
    }
    /*
    lifetime seconds { 0 | (sec-val) }
    */
    container lifetime {
      description
        "Configure lifetime for IKE SAs";
      leaf seconds {
        type enumeration {
          enum 0 {
            value 0;
            description
              "Enter lifetime 0 second for no timeout";
          }
          enum sec-val {
            value 1;
            description
              "IKE SA lifetime in seconds";
          }
        }
        description
          "Enter lifetime seconds for IKE SAs";
      }
    }
    /*
    identity { { local { (identity-ipv4) | dn | fqdn (identity-
     name) } } | { remote { (identity-ipv4) | fqdn (identity-
     name) | dn (identity-dnname) } } }
    */
    container identity {
      description
        "Specify IKE identity value";
      container local {
        description
          "Specify IKE identity local value";
        leaf identity-ipv4 {
          type inet:ipv4-address;
          description
            "IP Address";
        }
        leaf dn {
          type empty;
          description
            "Distinguished name in certificate";
        }
        leaf fqdn {
          type string;
          description
            "Fully qualified domain name";
        }
      }
      container remote {
        description
          "Specify IKE identity remote value";
        leaf identity-ipv4 {
          type inet:ipv4-address;
          description
            "IP Address";
        }
        leaf fqdn {
          type string;
          description
            "Fully qualified domain name";
        }
        leaf dn {
          type string;
          description
            "Distinguished name in certificate";
        }
      }
    }
    /*
    pre-shared-key { encrypted (encrypted-txt) | encrypted-hex
     (encrypted-hex-val) | hex (hex-val) | (ascii-val) }
    */
    container pre-shared-key {
      description
        "Specify IKE pre-shared-key value";
      choice pre-shared-key {
        description
          "IPOS choice";
        case encrypted {
          leaf encrypted {
            type string;
            description
              "Enter an already encrypted key input";
          }
        }
        case encrypted-hex {
          leaf encrypted-hex {
            type yang:hex-string;
            description
              "Encrypted Hexadecimal key input";
          }
        }
        case hex {
          leaf hex {
            type yang:hex-string;
            description
              "Hexadecimal key input";
          }
        }
        case ascii-val {
          leaf ascii-val {
            type string;
            description
              "Key string value";
          }
        }
      }
    }
    /*
    dpd interval (dpd-value)
    */
    container dpd {
      description
        "Configure the IKE DPD";
      leaf interval {
        type uint32 {
          range "1..86400";
        }
        description
          "Configure the IKE DPD interval";
      }
    }
    /*
    seq (seq-num) proposal (proposal-name)
    */
    container seq {
      status obsolete;
      description
        "Specify IKE proposal sequence number";
      leaf seq {
        type uint8 {
          range "1..8";
        }
        mandatory true;
        description
          "Sequence number";
      }
      leaf proposal {
        type string;
        mandatory true;
        description
          "Configure IKE proposal for the policy";
      }
    }
    /*
    sequence (seq-num) proposal (proposal-name)
    */
    list sequence {
      key "sequence";
      description
        "Specify IKE proposal sequence number";
      leaf sequence {
        type uint8 {
          range "1..8";
        }
        description
          "Sequence number";
      }
      container proposal {
        description
          "Configure IKE proposal for the policy";
        leaf proposal {
          type string;
          mandatory true;
          description
            "Specify an IKE proposal name";
        }
      }
    }
  }

  grouping ipsec-acl-grp {
    description
      "IPsec acl mode";
    /*
    description (desc-str)
    */
    leaf description {
      type string {
        length "1..256";
      }
      description
        "Enter the acl description text";
    }
    /*
    seq (seq-num) [ (proto-num) | tcp | udp ]  { (src-prefix) |
     (src-ipv6-prefix) | any | src-dynamic } [ src-eq (port-
     value) ] { (dst-prefix) | (dst-ipv6-prefix)  | dst-dynamic }
     [ dst-eq (port-value) ]
    */
    list seq {
      key "seq-num";
      description
        "Access list sequence number";
      leaf seq-num {
        type uint8 {
          range "1";
        }
        description
          "Sequence number";
      }
      choice seq-opt {
        description
          "IPOS choice";
        case proto-num {
          leaf proto-num {
            type uint8 {
              range "0..255";
            }
            description
              "Protocol Number";
          }
        }
        case tcp {
          leaf tcp {
            type empty;
            description
              "Transmission Control Protocol";
          }
        }
        case udp {
          leaf udp {
            type empty;
            description
              "User Datagram Protocol";
          }
        }
      }
      choice seq-choice1 {
        description
          "IPOS choice";
        case src-prefix {
          leaf src-prefix {
            type inet:ipv4-prefix;
            description
              "<IPv4 Prefix>/<Prefix Length>";
          }
        }
        case src-ipv6-prefix {
          leaf src-ipv6-prefix {
            type inet:ipv6-prefix;
            description
              "<IPv6 Prefix>/<Prefix Length>";
          }
        }
        case any {
          leaf any {
            type empty;
            description
              "Any source address";
          }
        }
        case src-dynamic {
          leaf src-dynamic {
            type empty;
            description
              "Dynamic source address";
          }
        }
      }
      leaf src-eq {
        when "(../../tcp or ../../udp or ../../proto-num=6 or "
        + "../proto-num=17)" {
          description
            "";
        }
        type uint16 {
          range "1..65535";
        }
        description
          "Equal to";
      }
      choice seq-choice2 {
        description
          "IPOS choice";
        case dst-prefix {
          leaf dst-prefix {
            type inet:ipv4-prefix;
            description
              "<IPv4 Prefix>/<Prefix Length>";
          }
        }
        case dst-ipv6-prefix {
          leaf dst-ipv6-prefix {
            type inet:ipv6-prefix;
            description
              "<IPv6 Prefix>/<Prefix Length>";
          }
        }
        case dst-dynamic {
          leaf dst-dynamic {
            type empty;
            description
              "Dynamic destination address";
          }
        }
      }
      leaf dst-eq {
        when "(../../../../tcp or ../../../../udp or "
        + "../../../../proto-num=6 or ../../../../proto-num=17)" {
          description
            "";
        }
        type uint16 {
          range "1..65535";
        }
        description
          "Equal to";
      }
    }
  }

  grouping ipsec-profile-grp {
    description
      "IPsec profile mode";
    /*
    seq (seq-num) { ipsec-policy (policy-name) } { access-group
     (acl-name) }
    */
    list seq {
      key "seq-num";
      description
        "Sequence number";
      leaf seq-num {
        type uint8 {
          range "1..8";
        }
        description
          "Sequence number";
      }
      list ipsec-policy {
        key "ipsec-policy";
        description
          "ipsec-policy";
        leaf ipsec-policy {
          type string;
          description
            "Specify IPsec policy name";
        }
      }
      list access-group {
        key "access-group";
        description
          "Enter IPsec access-list name";
        leaf access-group {
          type string;
          description
            "Specify IPsec access-list name";
        }
      }
    }
    /*
    df-bit { propagate | set | clear }
    */
    container df-bit {
      description
        "Configure Don't Fragment bit for IP header";
      choice df-bit {
        description
          "IPOS choice";
        case propagate {
          leaf propagate {
            type empty;
            description
              "Propagate DF bit for outer IP header";
          }
        }
        case set {
          leaf set {
            type empty;
            description
              "Set DF bit for outer IP header";
          }
        }
        case clear {
          leaf clear {
            type empty;
            description
              "Clear DF bit for outer IP header";
          }
        }
      }
    }
    /*
    mtu (mtu-value)
    */
    leaf mtu {
      type uint16 {
        range "256..1600";
      }
      description
        "Set the mtu";
    }
  }

  augment "/ctxsr6k:contexts/ctxr6k:context" {
    description
      "ericsson-ipsec";
    /*
    ipsec { access-list (acl-name) | profile (profile-name) |
     remote-access { dhcp-server (srv-ipaddr) relay (relay-
     ipaddr) | ip-pool (pool-prefix) | ipv6-pool (pool-prefix) }
     }
    */
    container ipsec {
      description
        "IPsec per context configuration command";
      list access-list {
        key "access-list";
        description
          "Configure the IPsec access-list";
        leaf access-list {
          type string;
          description
            "Specify an IPsec access-list name";
        }
        uses ipsec-acl-grp;
      }
      list profile {
        key "profile";
        description
          "Create an IPSec Profile.";
        leaf profile {
          type string;
          description
            "Name of the profile";
        }
        uses ipsec-profile-grp;
      }
      container remote-access {
        description
          "Configure the IPSec remote access options";
        container dhcp-server {
          presence "";
          description
            "container dhcp-server.";
          leaf dhcp-server {
            type inet:ipv4-address;
            mandatory true;
            description
              "Configure the IPSec remote access DHCP server";
          }
          leaf relay {
            type inet:ipv4-address;
            mandatory true;
            description
              "Configure the IPSec DHCP relay";
          }
        }
        leaf ip-pool {
          type inet:ipv4-prefix;
          description
            "Configure the IPSec remote access IP Pool";
        }
        leaf ipv6-pool {
          type inet:ipv6-prefix;
          description
            "Configure the IPSec remote access IPV6 Pool";
        }
      }
    }
    /*
    ike2 policy (policy-name)
    */
    container ike2 {
      description
        "IKEv2 per context configuration command";
      list policy {
        key "policy";
        description
          "Configure the IKE policy";
        leaf policy {
          type string;
          description
            "Specify an IKE policy name";
        }
        uses ike-policy-grp;
      }
    }
  }

  augment "/ctxsr6k:contexts" {
    description
      "ericsson-ipsec";
    /*
    ipsec { proposal (proposal-name) | policy (policy-name) }
    */
    container ipsec {
      description
        "Enter The IPsec Configuration";
      list proposal {
        key "proposal";
        description
          "Configure the IPsec proposal";
        leaf proposal {
          type string;
          description
            "Specify an IPsec proposal name";
        }
        uses ipsec-proposal-grp;
      }
      list policy {
        key "policy";
        description
          "Configure the IPsec policy";
        leaf policy {
          type string;
          description
            "Specify an IPsec policy name";
        }
        uses ipsec-policy-grp;
      }
    }
    /*
    ike2 proposal (proposal-name)
    */
    container ike2 {
      description
        "Enter The IKEv2 Configuration";
      list proposal {
        key "proposal";
        description
          "Configure the IKE proposal";
        leaf proposal {
          type string;
          description
            "Specify an IKE proposal name";
        }
        uses ike-proposal-grp;
      }
    }
  }

  augment "/ctxsr6k:contexts/tunnelrouter6000:tunnel-ipsec" {
    description
      "ericsson-ipsec";
    /*
    seq (seq-num) { ipsec-policy (policy-name) } { access-group
     (acl-name) }
    */
    list seq {
      key "seq-num";
      description
        "Sequence number";
      leaf seq-num {
        type uint8 {
          range "1..5";
        }
        description
          "Sequence number";
      }
      leaf ipsec-policy {
        type string;
        mandatory true;
        description
          "ipsec-policy";
      }
      leaf access-group {
        type string;
        mandatory true;
        description
          "Enter IPsec access-list name";
      }
    }
    /*
    alarms
    */
    leaf alarms {
      type empty;
      description
        "controls the generation of the tunnel state trap for
         each tunnel";
    }
    /*
    df-bit { propagate | set | clear }
    */
    container df-bit {
      description
        "Configure Don't Fragment bit for IP header";
      choice df-bit {
        description
          "IPOS choice";
        case propagate {
          leaf propagate {
            type empty;
            description
              "Propagate DF bit for outer IP header";
          }
        }
        case set {
          leaf set {
            type empty;
            description
              "Set DF bit for outer IP header";
          }
        }
        case clear {
          leaf clear {
            type empty;
            description
              "Clear DF bit for outer IP header";
          }
        }
      }
    }
    /*
    ike-policy (policy-name)
    */
    leaf ike-policy {
      type string;
      description
        "Configure ike-policy";
    }
    /*
    max-tunnels (number)
    */
    list max-tunnels {
      key "max-tunnels";
      description
        "Configure IPsec max-tunnels";
      leaf max-tunnels {
        type uint8 {
          range "1..16";
        }
        description
          "Specify IPsec access-list name";
      }
    }
  }

}
