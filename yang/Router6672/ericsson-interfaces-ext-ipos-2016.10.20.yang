module ericsson-interfaces-ext-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-interfaces-ext-ipos";

  prefix "ifxipos";

  import ietf-inet-types {
    prefix "inet";
  }

  import ericsson-types-ipos {
    prefix "typesipos";
  }

  import ietf-interfaces {
    prefix "if";
  }

  import ericsson-contexts-ipos {
    prefix "ctxsipos";
  }

  import ericsson-context-ipos {
    prefix "ctxipos";
  }

  import iana-if-type {
    prefix "ianaift";
  }

  organization
    "Ericsson";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-interfaces-ext-ipos
     Copyright (c) 2015-2016 Ericsson AB.
     All rights reserved";

  revision "2016-10-20" {
    description
      "Add the ipv4/v6 type to fix the ipv4 and v6 addresses
       can't be set successfully.";
    reference
      "rfc6020";
  }

  revision "2016-07-29" {
    description
      "remove the if-feature of ipv6 adress added on
       2016-07-08.";
    reference
      "rfc6020";
  }

  revision "2016-07-08" {
    description
      "remove ipv6 verify and secondary address.";
    reference
      "rfc6020";
  }

  revision "2016-03-29" {
    description
      "For CLI2Yang models data node sequence change.";
    reference
      "rfc6020";
  }

  revision "2015-12-07" {
    description
      "initial revision";
    reference
      "rfc6020";
  }

  feature multicast-output {
    description
      "This feature indicates that the device supports
       multicast-output in interface.";
  }

  feature clear-df {
    description
      "This feature indicates that the device supports clear-df
       in interface.";
  }

  feature mtu {
    description
      "This feature indicates that the device supports mtu in
       interface.";
  }

  feature snmp-dhcp-server {
    description
      "This feature indicates that the device supports snmp-
       dhcp-server in interface.";
  }

  feature snmp-flow-ip {
    description
      "This feature indicates that the device supports snmp-
       flow-ip in interface.";
  }

  feature snmp-icmp-time-exceed {
    description
      "This feature indicates that the device supports snmp-
       icmp-time-exceed in interface.";
  }

  feature igmp {
    description
      "This feature indicates that the device supports igmp in
       interface.";
  }

  feature trafic-index-accounting {
    description
      "This feature indicates that the device supports trafic-
       index-accounting in interface.";
  }

  feature icr {
    description
      "This feature indicates that the device supports icr in
       interface.";
  }

  feature ipv6-mtu {
    description
      "This feature indicates that the device supports ipv6 mtu
       in interface.";
  }

  feature packet-logging {
    description
      "This feature indicates that the device supports packet
       logging in interface.";
  }

  feature pbr {
    description
      "This feature indicates that the device supports pbr in
       interface.";
  }

  feature ipv6 {
    description
      "This feature indicates that the device supports ipv6
       address in interface.";
  }

  grouping if-grp {
    description
      "Interface configuration mode";
    /*
    multicast output [ accept-unknown-mac ]
    */
    container multicast-output {
      if-feature multicast-output;
      presence "";
      description
        "Multicast interface configuration Enable interface for
         sRMR Multicast output";
      leaf accept-unknown-mac {
        type empty;
        description
          "Accept joins from unknown MAC addresses";
      }
    }
    /*
    ipv6 { mtu (mtu-len) | link-local [ (local-addr) ] | {
     address { addr-primary (addr) | addr-secondary (addr) } |
     unnumbered (unnumbered-if) } | < access-group (cls-name)
     (direction) [ count ] [ log ] > | verify unicast source
     reachable-via { any | receive }  [ allow-default ] [ access-
     group (access-list) [ acl-count] ]}
    */
    container ipv6 {
      description
        "IPv6 configuration command";
      leaf mtu {
        if-feature ipv6-mtu;
        type uint16 {
          range "1280..16384";
        }
        description
          "Set IP mtu size Enter mtu length";
      }
      container link-local {
        presence "";
        description
          "Configure a IPV6 link local address";
        leaf local-addr {
          type inet:ipv6-address;
          description
            "Link local IPv6 address";
        }
      }
      choice ipv6-choice {
        description
          "IPOS choice";
        case address {
          container address {
            description
              "IP address to be configured";
            leaf addr-primary {
              type typesipos:ipv6-address-mask;
              description
                "IP address to be configured IPv6 address";
            }
            list addr-secondary {
              key "addr-secondary";
              description
                "IP address to be configured";
              leaf addr-secondary {
                type typesipos:ipv6-address-mask;
                must "../../addr-primary" {
                  error-message "Configure a primary IP address "
                  + "before configuring a v6 secondary address";
                  description
                    "";
                }
                description
                  "IPv6 address";
              }
            }
          }
        }
        case unnumbered {
          leaf unnumbered {
            type leafref {
              path "/if:interfaces/if:interface/if:name";
            }
            description
              "IP unnumbered: address to be borrowed from
               another interface Specify a named interface to
               borrow ip address from";
          }
        }
      }
      list access-group {
        key "direction";
        description
          "Apply packet access control";
        leaf direction {
          type typesipos:access_group_dir;
          description
            "The direction ACL list to appiled in the interface";
        }
        leaf cls-name {
          type string {
            pattern '(\S+)|("\S+(\s\S+)*")';
          }
          description
            "access list name (up to 10 names)";
        }
        leaf count {
          type empty;
          description
            "Configure access list packet counting";
        }
        leaf log {
          if-feature packet-logging;
          type empty;
          description
            "Configure access list packet logging";
        }
      }
      container verify-unicast-source-reachable-via {
        if-feature pbr;
        description
          "Enable per packet validation Enable per packet
           validation for unicast Enable source IPv6 address
           validation Specify the validation for the source
           address";
        leaf verify-unicast-source-reachable-via-choice {
          type enumeration {
            enum any {
              description
                "Source IPv6 address can be reached by any
                 interface";
            }
            enum receive {
              description
                "Source address must be reachable through the
                 incoming interface";
            }
          }
          description
            "leaf verify-unicast-source-reachable-via-choice.";
        }
        leaf allow-default {
          type empty;
          description
            "Allow RPF to lookup the default route for
             verification";
        }
        container access-group {
          presence "";
          description
            "Apply access control to RPF IPv6 source address
             selection";
          leaf access-list {
            type string;
            mandatory true;
            description
              "IPv6 access list name(s) (up to 10 enclosed in
               quotes)";
          }
          leaf acl-count {
            type empty;
            description
              "Enable IPv6 access list match counting";
          }
        }
      }
    }
    /*
    ip { mtu (mtu-len) | clear-df | { < address { addr-primary
     (addr) [ tag  (tag-num)  ] | < addr-secondary (addr)  [ tag
     (tag-num)  ]> }  > | unnumbered (unnumbered-if) }  | <
     access-group  (cls-name) (direction) [ count ] [ log ] > |
     verify unicast source reachable-via { any | receive }  [
     allow-default ] [ access-group (access-list) [ acl-count] ]
     }
    */
    container ip {
      description
        "IP configuration command";
      leaf mtu {
        if-feature mtu;
        type uint16 {
          range "256..16384";
        }
        description
          "Set IP mtu size Enter mtu length";
      }
      leaf clear-df {
        if-feature clear-df;
        type empty;
        description
          "Ignore DF bit on IP packets and fragment if needed";
      }
      choice ip-choice {
        description
          "IPOS choice";
        case address {
          container address {
            description
              "IP address to be configured";
            container addr-primary {
              presence "";
              description
                "IP address to be configured";
              leaf addr {
                type typesipos:ipos-ip-prefix;
                mandatory true;
                description
                  "Address followed by mask or prefix length";
              }
              leaf tag {
                type uint32 {
                  range "0..4294967295";
                }
                default "0";
                description
                  "Route tag tag value";
              }
            }
            list addr-secondary {
              must "../addr-primary/addr" {
                error-message "Configure a primary IP address "
                + "before configuring a v4 secondary address";
                description
                  "Configure a primary IP address before
                   configuring a secondary address";
              }
              key "addr";
              max-elements 15;
              description
                "IP address to be configured";
              leaf addr {
                type typesipos:ipos-ip-prefix;
                description
                  "Address followed by mask or prefix length";
              }
              leaf tag {
                type uint32 {
                  range "0..4294967295";
                }
                default "0";
                description
                  "Route tag tag value";
              }
            }
          }
        }
        case unnumbered {
          leaf unnumbered {
            type leafref {
              path "/if:interfaces/if:interface/if:name";
            }
            description
              "IP unnumbered: address to be borrowed from
               another interface Specify a named interface to
               borrow ip address from";
          }
        }
      }
      list access-group {
        key "direction";
        description
          "Apply packet access control";
        leaf direction {
          type typesipos:access_group_dir;
          description
            "The direction ACL list to appiled in the interface";
        }
        leaf cls-name {
          type string {
            pattern '(\S+)|("\S+(\s\S+)*")';
          }
          description
            "access list name (up to 10 names); more than one
             ACL enter with \"\" and space separator";
        }
        leaf count {
          type empty;
          description
            "Configure access list packet counting";
        }
        leaf log {
          if-feature packet-logging;
          type empty;
          description
            "Configure access list packet logging";
        }
      }
      container verify-unicast-source-reachable-via {
        description
          "Enable per packet validation Enable per packet
           validation for unicast Enable source IPv6 address
           validation Specify the validation for the source
           address";
        leaf verify-unicast-source-reachable-via-choice {
          type enumeration {
            enum any {
              description
                "Source IPv6 address can be reached by any
                 interface";
            }
            enum receive {
              description
                "Source address must be reachable through the
                 incoming interface";
            }
          }
          description
            "leaf verify-unicast-source-reachable-via-choice.";
        }
        leaf allow-default {
          type empty;
          description
            "Allow RPF to lookup the default route for
             verification";
        }
        container access-group {
          presence "";
          description
            "Apply access control to RPF IPv6 source address
             selection";
          leaf access-list {
            type string;
            mandatory true;
            description
              "IPv6 access list name(s) (up to 10 enclosed in
               quotes)";
          }
          leaf acl-count {
            type empty;
            description
              "Enable IPv6 access list match counting";
          }
        }
      }
    }
    /*
    icr { link-protection | transport | prefix-advertisement [
     tracking ] }
    */
    container icr {
      if-feature icr;
      description
        "ICR (Inter-Chassis Resilience) configuration";
      choice icr {
        description
          "IPOS choice";
        case link-protection {
          leaf link-protection {
            type empty;
            description
              "Configure multi-chassis-link-protection";
          }
        }
        case transport {
          leaf transport {
            type empty;
            description
              "Configure icr transport for MC-LAG";
          }
        }
        case prefix-advertisement {
          container prefix-advertisement {
            presence "";
            description
              "Monitor the prefix route advertised";
            leaf tracking {
              type empty;
              description
                "Enable ICR tracking";
            }
          }
        }
      }
    }
    /*
    traffic-index accounting
    */
    leaf traffic-index-accounting {
      if-feature trafic-index-accounting;
      type empty;
      description
        "Enable traffic-index accounting Enable traffic-index
         accounting";
    }
    /*
    context (ctx-name)
    */
    leaf context {
      type leafref {
        path "/ctxsipos:contexts/ctxipos:context/ctxipos"
        + ":context-name";
      }
      mandatory true;
      description
        "Name of context which the interface belongs to Name of
         context which the interface belongs to";
    }
  }

  augment "/if:interfaces/if:interface" {
    description
      "ericsson-interface";
    /*
    l3-interface
    */
    container l3-interface {
      when "../if:type = 'ianaift:ipForward' or ../if:type = "
      + "'ianaift:softwareLoopback'" {
        description
          "";
      }
      presence "";
      description
        "Configure a logical interface";
      uses if-grp;
    }
  }

}
