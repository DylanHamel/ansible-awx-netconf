module ericsson-ethernet-ext-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-ethernet-ext-ipos";

  prefix "ethxipos";

  import ericsson-context-ipos {
    prefix "ctxipos";
  }

  import ietf-interfaces {
    prefix "if";
  }

  import iana-if-type {
    prefix "ianaift";
  }

  import ericsson-contexts-ipos {
    prefix "ctxsipos";
  }

  import ietf-inet-types {
    prefix "inet";
  }

  import ietf-yang-types {
    prefix "yang";
  }

  organization
    "Ericsson";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-ethernet-ext-ipos
     Copyright (c) 2015-2016 Ericsson AB.
     All rights reserved";

  revision "2016-03-29" {
    description
      "For CLI2Yang models data node sequence change.";
    reference
      "rfc6020";
  }

  revision "2016-03-16" {
    description
      "support management port";
    reference
      "rfc6020";
  }

  revision "2015-12-07" {
    description
      "initial revision";
    reference
      "rfc6020";
  }

  feature ipfix-apply {
    description
      "This feature indicates that the device supports ipfix-
       apply in ethernet.";
  }

  feature ip-host {
    description
      "This feature indicates that the device supports ip-host
       in ethernet.";
  }

  feature igmp {
    description
      "This feature indicates that the device supports igmp in
       ethernet.";
  }

  feature auto-negotiate-duplex {
    description
      "This feature indicates that the device supports auto-
       negotiate-duplex in ethernet.";
  }

  feature loopback {
    description
      "This feature indicates that the device supports loopback
       in ethernet.";
  }

  feature gre-interafce {
    description
      "This feature indicates that the device supports gre-
       interafce in ethernet.";
  }

  feature clips {
    description
      "This feature indicates that the device supports clips in
       ethernet.";
  }

  feature flow-apply {
    description
      "This feature indicates that the device supports flow-
       apply in ethernet.";
  }

  feature multicast {
    description
      "This feature indicates that the device supports multicast
       in ethernet.";
  }

  grouping port-grp {
    description
      "Port configuration mode";
    /*
    ipfix apply { { ipv4 | ipv6 } { monitor (monitor-name) } {
     sampler (sampler-name) } { in | out } | mpls monitor }
    */
    container ipfix-apply {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      if-feature ipfix-apply;
      presence "";
      description
        "Configure ipfix apply command in port context Apply a
         monitor and sampler to a circuit";
      choice ipfix-apply {
        description
          "IPOS choice";
        case ipos-choice {
          container ipos-choice {
            description
              "container ipos-choice.";
            leaf ipos-choice-choice {
              type enumeration {
                enum ipv4 {
                  description
                    "IPV4 packets to sample";
                }
                enum ipv6 {
                  description
                    "IPV6 packets to sample";
                }
              }
              mandatory true;
              description
                "leaf ipos-choice-choice.";
            }
            leaf monitor {
              type string;
              mandatory true;
              description
                "Name of the monitor configuration Monitor name";
            }
            leaf sampler {
              type string;
              mandatory true;
              description
                "Name of the sampler configuration Enter Sampler
                 name";
            }
            leaf ipos-choice-choice1 {
              type enumeration {
                enum in {
                  description
                    "Ingress direction, Sample Incoming packets";
                }
                enum out {
                  description
                    "Egress direction, Sample outgoing packets";
                }
              }
              mandatory true;
              description
                "leaf ipos-choice-choice1.";
            }
          }
        }
        case mpls-monitor {
          leaf mpls-monitor {
            type empty;
            description
              "MPLS packets to sample Name of the monitor
               configuration";
          }
        }
      }
    }
    /*
    link-group (lg-name)
    */
    leaf link-group {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      type leafref {
        path "/if:interfaces/if:interface/if:name";
      }
      description
        "Specify the link-group Name of the link-group";
    }
    /*
    lacp priority (priority-val)
    */
    container lacp {
      description
        "Configure LACP parameters";
      leaf priority {
        type uint16 {
          range "0..65535";
        }
        default "32767";
        description
          "Configure LACP port priority Specify the priority";
      }
    }
    /*
    synchronous-mode
    */
    leaf synchronous-mode {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      type empty;
      description
        "Enable synchronous mode";
    }
    /*
    bind interface (intf-name) (intf-ctx)
    */
    container bind-interface {
      presence "";
      description
        "Bind Port Bind to an interface";
      leaf intf-name {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        mandatory true;
        description
          "Interface to bind to";
      }
      leaf intf-ctx {
        type leafref {
          path "/ctxsipos:contexts/ctxipos:context/ctxipos"
          + ":context-name";
        }
        mandatory true;
        description
          "Context name to be bound";
      }
    }
    /*
    ip host { (route-prefix) | (host-addr) [ (host-mac) ] }
    */
    container ip-host {
      if-feature ip-host;
      presence "";
      description
        "Configure IP properties on this circuit Configure IP
         Host on this circuit";
      choice ip-host {
        description
          "IPOS choice";
        case route-prefix {
          leaf route-prefix {
            type inet:ipv4-prefix;
            description
              "Specify the destination subnet on this circuit";
          }
        }
        case host-addr {
          container host-addr {
            description
              "container host-addr.";
            leaf host-addr {
              type inet:ipv4-address;
              mandatory true;
              description
                "IP Address of the remote host on this circuit";
            }
            leaf host-mac {
              type yang:mac-address;
              description
                "MAC-Address of the host on this circuit";
            }
          }
        }
      }
    }
    /*
    flow apply { < admission-control { profile (prof-name) } {
     in | out | bidirectional } > | < ip { profile (profile-name)
     } { in | out | both } >}
    */
    container flow-apply {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      if-feature flow-apply;
      presence "";
      description
        "Configure Flows on this circuit Apply Flows on this
         circuit";
      container admission-control {
        description
          "Apply Flow Admission-Control on this circuit";
        leaf profile {
          type string;
          mandatory true;
          description
            "Apply a Flow AC profile to this circuit Specify the
             name of a Flow Admission-Control profile";
        }
        leaf admission-control-choice {
          type enumeration {
            enum in {
              description
                "Apply FAC profile for ingress traffic";
            }
            enum out {
              description
                "Apply FAC profile for egress traffic";
            }
            enum bidirectional {
              description
                "Apply FAC profile for bidirectional traffic";
            }
          }
          mandatory true;
          description
            "leaf admission-control-choice.";
        }
      }
      container ip {
        description
          "IP flow configuration";
        leaf profile {
          type string;
          mandatory true;
          description
            "IP flow profile configuration Profile name";
        }
        leaf ip-choice {
          type enumeration {
            enum in {
              description
                "Apply profile in ingress direction only";
            }
            enum out {
              description
                "Apply profile in egress direction only";
            }
            enum both {
              description
                "Apply profile in ingress and egress directions";
            }
          }
          mandatory true;
          description
            "leaf ip-choice.";
        }
      }
    }
    /*
    clips pvc (source-cnt) [ through (source-erange) ]
    */
    container clips-pvc {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      presence "";
      description
        "Create static CLIPs pvcs Create static CLIPs pvcs";
      leaf source-cnt {
        type uint32 {
          range "1..131072";
        }
        mandatory true;
        description
          "Session id ( Max Sessions : 48000 )";
      }
      leaf through {
        type uint32 {
          range "3..131072";
        }
        description
          "Specify a range of session ids ( Max Sessions : 48000
           ) End range of session Ids ( Max Sessions : 48000 )";
      }
    }
    /*
    ipv6 multicast maximum-bandwidth [ (bw-val) [ percent | Kbps
     | Mbps | Gbps ] ]
    */
    container ipv6-multicast-maximum-bandwidth {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      if-feature multicast;
      presence "";
      description
        "Set ipv6 multicast attribute Set multicast attribute
         Set max-bandwidth for subscribers";
      container ipv6-multicast-maximum-bandwidth-opt {
        description
          "container ipv6-multicast-maximum-bandwidth-opt.";
        leaf bw-val {
          type uint16 {
            range "1..65535";
          }
          mandatory true;
          description
            "max-bandwidth in Kbps/Mbps/Gbps/percent";
        }
        choice ipv6-multicast-maximum-bandwidth-opt-opt {
          description
            "IPOS choice";
          case percent {
            leaf percent {
              type empty;
              description
                "Input in percentage form";
            }
          }
          case Kbps {
            leaf Kbps {
              type empty;
              description
                "Kilo bits per sec";
            }
          }
          case Mbps {
            leaf Mbps {
              type empty;
              description
                "Mega bits per sec";
            }
          }
          case Gbps {
            leaf Gbps {
              type empty;
              description
                "Giga bits per sec";
            }
          }
        }
      }
    }
    /*
    mtu (mtu-number)
    */
    leaf mtu {
      type uint16 {
        range "256..9600";
      }
      default "1500";
      description
        "Change media mtu size
                        Maximum Transfer Unit (MTU) in bytes
                      ";
    }
    /*
    gre interface (interface-name) (context-name)
    */
    container gre-interface {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      if-feature gre-interafce;
      presence "";
      description
        "Configure gre interface Configure gre interface";
      leaf interface-name {
        type string;
        mandatory true;
        description
          "Configure a named interface";
      }
      leaf context-name {
        type string;
        mandatory true;
        description
          "Context name";
      }
    }
    /*
    encapsulation dot1q
    */
    leaf encapsulation-dot1q {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      type empty;
      description
        "Set encapsulation dot1q";
    }
    /*
    link-dampening [ {up (up-delay)} {down (down-delay)} [
     restart (restart-delay) ] ]
    */
    container link-dampening {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      presence "";
      description
        "Dampen the line state detection to reduce port flaps";
      container link-dampening-opt {
        presence "";
        description
          "container link-dampening-opt.";
        leaf up {
          type uint16 {
            range "0..65535";
          }
          default "10000";
          description
            "Set link-up dampening delay Link-up delay time
             (ms)";
        }
        leaf down {
          type uint16 {
            range "0..65535";
          }
          default "2500";
          description
            "Set link-down dampening delay Link-down delay time
             (ms)";
        }
        leaf restart {
          type uint16 {
            range "0..65535";
          }
          default "0";
          description
            "Set link-up dampening delay on system restart
             restart delay time (seconds)";
        }
      }
    }
    /*
    auto-negotiate [ flc  {off | tx-only  | tx&rx | tx&rx-or-rx-
     only } | speed { 1000 | 100 | 10 | 10-or-100} | duplex {
     full | half  | full-or-half } | force { enable | disable} ]
    */
    container auto-negotiate {
      description
        "Set auto negotiation parameters";
      leaf auto-negotiate-enable {
        type boolean;
        default "true";
        description
          "Enable/Disable auto-negotiate-enable";
      }
      leaf flc {
        type enumeration {
          enum off {
            description
              "Set flow-control negotiated set to 'off'";
          }
          enum tx-only {
            description
              "Set flow-control negotiated set to 'Wish to
               Send'";
          }
          enum tx&rx {
            description
              "Set flow-control negotiated set to 'Wish to Send
               and Receive'";
          }
          enum tx&rx-or-rx-only {
            description
              "Set flow-control negotiated set to 'Wish to Send
               and Receive, or just Receive'";
          }
        }
        default "off";
        description
          "Set flow-control negotiated set";
      }
      leaf speed {
        type enumeration {
          enum 1000 {
            description
              "Set port speed negotiated set to 1000 Mbps";
          }
          enum 100 {
            description
              "Set port speed negotiated set to 100 Mbps";
          }
          enum 10 {
            description
              "Set port speed negotiated set to 10 Mbps";
          }
          enum 10-or-100 {
            description
              "Set port speed negotiated set to 10 Mpbs or 100
               Mbps";
          }
        }
        description
          "Set port speed negotiated set";
      }
      leaf duplex {
        if-feature auto-negotiate-duplex;
        type enumeration {
          enum full {
            description
              "Set port duplex mode negotiated set to full
               duplex";
          }
          enum half {
            description
              "Set port duplex mode negotiated set to half
               duplex";
          }
          enum full-or-half {
            description
              "Set port duplex mode negotiated set to full
               duplex or half duplex";
          }
        }
        description
          "Set port duplex mode negotiated set";
      }
      leaf force {
        type enumeration {
          enum enable {
            description
              "
                              Force port UP if auto negotiation
               fails
                            ";
          }
          enum disable {
            description
              "
                              Set port to remain DOWN if auto
               negotiation fails
                            ";
          }
        }
        default "disable";
        description
          " Set port state if auto negotiation fails ";
      }
    }
    /*
    duplex { full | half }
    */
    leaf duplex {
      type enumeration {
        enum full {
          description
            "Set port duplex mode to full duplex";
        }
        enum half {
          description
            "Set port duplex mode to half duplex";
        }
      }
      description
        "Set port duplex mode";
    }
    /*
    speed { 1000 | 100  }
    */
    leaf speed {
      type enumeration {
        enum 1000 {
          description
            "Set port speed to 1000 Mbps";
        }
        enum 100 {
          description
            "Set port speed to 100 Mbps";
        }
      }
      description
        "Set port speed";
    }
    /*
    flow-control [rx|tx|tx&rx]
    */
    container flow-control {
      description
        "Enable/Disable flow control";
      leaf flow-control-enable {
        type boolean;
        default "true";
        description
          "Enable/Disable flow-control-enable";
      }
      leaf flow-control-opt {
        type enumeration {
          enum rx {
            description
              "Set flow-control for Receive only";
          }
          enum tx {
            description
              "Set flow-control for Send only";
          }
          enum tx&rx {
            description
              "Set flow-control for both Send and Receive";
          }
        }
        default "rx";
        description
          "leaf flow-control-opt.";
      }
    }
    /*
    loopback
    */
    leaf loopback {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      if-feature loopback;
      type empty;
      description
        "Enable loopback";
    }
    /*
    igmp maximum-bandwidth (bandwidth-rate) [ percent ]
    */
    container igmp-maximum-bandwidth {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      presence "";
      description
        "Configure IGMP related parameters Specify maximum
         bandwidth for a port";
      leaf bandwidth-rate {
        type uint16 {
          range "1..65535";
        }
        mandatory true;
        description
          "Rate in kbps";
      }
      leaf percent {
        type empty;
        description
          "Specfied rate is percentage of port bandwidth";
      }
    }
    /*
    storm-control { pps | kbps }
    */
    container storm-control {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      description
        "Configure storm-control parameters";
      choice storm-control {
        description
          "IPOS choice";
        case pps {
          container pps {
            presence "";
            description
              "Packets-per-second";
            uses port-storm-ctrl-pps-grp;
          }
        }
        case kbps {
          container kbps {
            presence "";
            description
              "Kilobits-per-second";
            uses port-storm-ctrl-kbps-grp;
          }
        }
      }
    }
    /*
    port-type { wan-phy | 1ge | 10ge }
    */
    leaf port-type {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      type enumeration {
        enum wan-phy {
          description
            "Select WAN-PHY as port physical layer type";
        }
        enum 1ge {
          description
            "Select 1GE port type";
        }
        enum 10ge {
          description
            "Select 10GE port type";
        }
      }
      description
        "The physical type of the port";
    }
  }

  grouping port-storm-ctrl-kbps-grp {
    description
      "storm control configuration parameters of kbps";
    /*
    unknown-dest { rate (rate-range) burst (size-range) }
    */
    container unknown-dest {
      presence "";
      description
        "Unknown destination frame storm-control parameters";
      leaf rate {
        type uint32 {
          range "122..1000000";
        }
        mandatory true;
        description
          "Rate limit in kbps ";
      }
      leaf burst {
        type uint32 {
          range "10000..4000000";
        }
        mandatory true;
        description
          "Burst size limit in byte ";
      }
    }
    /*
    multicast { rate (rate-range) burst (size-range) }
    */
    container multicast {
      presence "";
      description
        "Multicast frame storm-control parameters";
      leaf rate {
        type uint32 {
          range "122..1000000";
        }
        mandatory true;
        description
          "Rate limit in kbps ";
      }
      leaf burst {
        type uint32 {
          range "10000..4000000";
        }
        mandatory true;
        description
          "Burst size limit in byte ";
      }
    }
    /*
    broadcast { rate (rate-range) burst (size-range) }
    */
    container broadcast {
      presence "";
      description
        "Broadcast frame storm-control parameters";
      leaf rate {
        type uint32 {
          range "122..1000000";
        }
        mandatory true;
        description
          "Rate limit in kbps ";
      }
      leaf burst {
        type uint32 {
          range "10000..4000000";
        }
        mandatory true;
        description
          "Burst size limit in byte ";
      }
    }
  }

  grouping port-storm-ctrl-pps-grp {
    description
      "storm control configuration parameters of pps";
    /*
    unknown-dest-rate (rate-range)
    */
    leaf unknown-dest-rate {
      type uint32 {
        range "238..2000000";
      }
      description
        "Unknown destination frame storm-control parameter, rate
         limit in pps ";
    }
    /*
    multicast-rate (rate-range)
    */
    leaf multicast-rate {
      type uint32 {
        range "238..2000000";
      }
      description
        "Multicast frame storm-control parameter, rate limit in
         pps ";
    }
    /*
    broadcast-rate (rate-range)
    */
    leaf broadcast-rate {
      type uint32 {
        range "238..2000000";
      }
      description
        "Broadcast frame storm-control parameter, rate limit in
         pps ";
    }
  }

  augment "/if:interfaces/if:interface" {
    description
      "ericsson-ethernet";
    /*
    ethernet
    */
    container ethernet {
      when "../if:type = 'ianaift:ethernetCsmacd'" {
        description
          "";
      }
      presence "";
      description
        "Select port to configure";
      uses port-grp;
    }
  }

}
