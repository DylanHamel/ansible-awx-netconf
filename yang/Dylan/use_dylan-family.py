#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import dylan_family


sw = dylan_family.dylan_family()


dylan = sw.family.individual.add("Dylan")
dylan.sex = "MALE"
dylan.age = 26

drogba = sw.family.individual.add("Didier Drogba")
drogba.sex = "MALE"
drogba.age = 42

print(json.dumps(sw.get(), indent=4))
