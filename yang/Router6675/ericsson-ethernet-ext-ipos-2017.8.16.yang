module ericsson-ethernet-ext-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-ethernet-ext-ipos";

  prefix "ethxipos";

  import ericsson-context-ipos {
    prefix "ctxipos";
  }

  import ietf-interfaces {
    prefix "if";
  }

  import iana-if-type {
    prefix "ianaift";
  }

  import ericsson-contexts-ipos {
    prefix "ctxsipos";
  }

  import ietf-inet-types {
    prefix "inet";
  }

  import ietf-yang-types {
    prefix "yang";
  }

  import ericsson-mirror-policy-ipos {
    prefix "mirrorpolicyipos";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-ethernet-ext-ipos
     Copyright (c) 2017 Ericsson AB.
     All rights reserved";

  revision "2017-08-16" {
    description
      "New command 100GE for port-type";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "1";
  }

  revision "2017-07-20" {
    description
      "IPOS 9349 Route-prefix and host-addr can be configured
       together and 'host-addr' can be configured multiple times";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  revision "2017-06-06" {
    description
      "IPOS 11281 add mirror policy in port ethernet";
    reference
      "rfc6020";
  }

  revision "2017-04-05" {
    description
      "IPOS 8033 [interface]The model ip access-group in
       interface have some issue.";
    reference
      "rfc6020";
  }

  revision "2017-04-05" {
    description
      "IPOS-8625 deviate percent and gre-interface from
       ericsson-ethernet-ext-ipos model.";
    reference
      "rfc6020";
  }

  revision "2017-01-11" {
    description
      "For default value of auto-negotiate changed.";
    reference
      "rfc6020";
  }

  revision "2016-12-30" {
    description
      "For Side Effect difference modification.";
    reference
      "rfc6020";
  }

  revision "2016-03-29" {
    description
      "For CLI2Yang models data node sequence change.";
    reference
      "rfc6020";
  }

  revision "2016-03-16" {
    description
      "support management port";
    reference
      "rfc6020";
  }

  revision "2015-12-07" {
    description
      "initial revision";
    reference
      "rfc6020";
  }

  feature ipfix-apply {
    description
      "This feature indicates that the device supports ipfix-
       apply in ethernet.";
  }

  feature ip-host {
    description
      "This feature indicates that the device supports ip-host
       in ethernet.";
  }

  feature igmp {
    description
      "This feature indicates that the device supports igmp in
       ethernet.";
  }

  feature auto-negotiate-duplex {
    description
      "This feature indicates that the device supports auto-
       negotiate-duplex in ethernet.";
  }

  feature loopback {
    description
      "This feature indicates that the device supports loopback
       in ethernet.";
  }

  feature gre-interafce {
    description
      "This feature indicates that the device supports gre-
       interafce in ethernet.";
  }

  feature clips {
    description
      "This feature indicates that the device supports clips in
       ethernet.";
  }

  feature flow-apply {
    description
      "This feature indicates that the device supports flow-
       apply in ethernet.";
  }

  feature multicast {
    description
      "This feature indicates that the device supports multicast
       in ethernet.";
  }

  grouping port-grp {
    description
      "Port configuration mode";
    /*
    ipfix apply { { ipv4 | ipv6 } { monitor (monitor-name) } {
     sampler (sampler-name) } { in | out } | mpls monitor }
    */
    container ipfix-apply {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      if-feature ipfix-apply;
      presence "";
      description
        "Apply a monitor and sampler to a circuit";
      choice ipfix-apply {
        description
          "IPOS choice";
        case ipos-choice {
          container ipos-choice {
            description
              "container ipos-choice.";
            leaf ipos-choice-choice {
              type enumeration {
                enum ipv4 {
                  value 0;
                  description
                    "IPV4 packets to sample";
                }
                enum ipv6 {
                  value 1;
                  description
                    "IPV6 packets to sample";
                }
              }
              mandatory true;
              description
                "leaf ipos-choice-choice.";
            }
            leaf monitor {
              type string;
              mandatory true;
              description
                "Name of the monitor configuration";
            }
            leaf sampler {
              type string;
              mandatory true;
              description
                "Name of the sampler configuration";
            }
            leaf ipos-choice-choice1 {
              type enumeration {
                enum in {
                  value 0;
                  description
                    "Ingress direction, Sample Incoming packets";
                }
                enum out {
                  value 1;
                  description
                    "Egress direction, Sample outgoing packets";
                }
              }
              mandatory true;
              description
                "leaf ipos-choice-choice1.";
            }
          }
        }
        case mpls-monitor {
          leaf mpls-monitor {
            type empty;
            description
              "Name of the monitor configuration";
          }
        }
      }
    }
    /*
    link-group (lg-name)
    */
    leaf link-group {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      type leafref {
        path "/if:interfaces/if:interface/if:name";
      }
      description
        "Specify the link-group";
    }
    /*
    lacp priority (priority-val)
    */
    container lacp {
      description
        "Configure LACP parameters";
      leaf priority {
        type uint16 {
          range "0..65535";
        }
        default "32767";
        description
          "Configure LACP port priority";
      }
    }
    /*
    synchronous-mode
    */
    leaf synchronous-mode {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      type empty;
      description
        "Enable synchronous mode";
    }
    /*
    bind interface (intf-name) (intf-ctx)
    */
    container bind-interface {
      presence "";
      description
        "Bind to an interface";
      leaf intf-name {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        mandatory true;
        description
          "Interface to bind to";
      }
      leaf intf-ctx {
        type leafref {
          path "/ctxsipos:contexts/ctxipos:context/ctxipos"
          + ":context-name";
        }
        mandatory true;
        description
          "Context name to be bound";
      }
    }
    /*
    ip host { (route-prefix) | (host-addr) } [ (host-mac) ]
    */
    list ip-host {
      if-feature ip-host;
      key "ip-host-choice";
      description
        "Configure IP Host on this circuit";
      leaf ip-host-choice {
        type union {
          type inet:ipv4-prefix;
          type inet:ipv4-address;
        }
        description
          "leaf ip-host-choice.";
      }
      leaf host-mac {
        type yang:mac-address;
        description
          "MAC-Address of the host on this circuit didn't
           support ipv4-prefix";
      }
    }
    /*
    flow apply { < admission-control { profile (prof-name) } {
     in | out | bidirectional } > | < ip { profile (profile-name)
     } { in | out | both } >}
    */
    container flow-apply {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      if-feature flow-apply;
      presence "";
      description
        "Apply Flows on this circuit";
      container admission-control {
        description
          "Apply Flow Admission-Control on this circuit";
        leaf profile {
          type string;
          mandatory true;
          description
            "Apply a Flow AC profile to this circuit";
        }
        leaf admission-control-choice {
          type enumeration {
            enum in {
              value 0;
              description
                "Apply FAC profile for ingress traffic";
            }
            enum out {
              value 1;
              description
                "Apply FAC profile for egress traffic";
            }
            enum bidirectional {
              value 2;
              description
                "Apply FAC profile for bidirectional traffic";
            }
          }
          mandatory true;
          description
            "leaf admission-control-choice.";
        }
      }
      container ip {
        description
          "IP flow configuration";
        leaf profile {
          type string;
          mandatory true;
          description
            "IP flow profile configuration";
        }
        leaf ip-choice {
          type enumeration {
            enum in {
              value 0;
              description
                "Apply profile in ingress direction only";
            }
            enum out {
              value 1;
              description
                "Apply profile in egress direction only";
            }
            enum both {
              value 2;
              description
                "Apply profile in ingress and egress directions";
            }
          }
          mandatory true;
          description
            "leaf ip-choice.";
        }
      }
    }
    /*
    clips pvc (source-cnt) [ through (source-erange) ]
    */
    container clips-pvc {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      if-feature clips;
      presence "";
      description
        "Create static CLIPs pvcs";
      leaf source-cnt {
        type uint32 {
          range "1..131072";
        }
        mandatory true;
        description
          "Session id ( Max Sessions : 48000 )";
      }
      leaf through {
        type uint32 {
          range "3..131072";
        }
        description
          "Specify a range of session ids ( Max Sessions : 48000
           )";
      }
    }
    /*
    subscribe micro-bfd
    */
    leaf subscribe-micro-bfd {
      type empty;
      description
        "Micro-bfd events for the link group";
    }
    /*
    mtu (mtu-number)
    */
    leaf mtu {
      type uint16 {
        range "256..9600";
      }
      default "1500";
      description
        "Change media mtu size";
    }
    /*
    encapsulation dot1q
    */
    leaf encapsulation-dot1q {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      type empty;
      description
        "dot1q";
    }
    /*
    link-dampening [ {up (up-delay)} {down (down-delay)} [
     restart (restart-delay) ] ]
    */
    container link-dampening {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      presence "";
      description
        "Dampen the line state detection to reduce port flaps";
      container link-dampening-opt {
        presence "";
        description
          "container link-dampening-opt.";
        leaf up {
          type uint16 {
            range "0..65535";
          }
          must "((number(../down)!=0) or (number(.)!=0))" {
            error-message "the value of up-delay and down-delay "
            + "both can not be zero.";
            description
              "the value of up-delay and down-delay both can not
               be 0";
          }
          default "10000";
          description
            "Set link-up dampening delay";
        }
        leaf down {
          type uint16 {
            range "0..65535";
          }
          must "((number(../up)!=0) or (number(.)!=0))" {
            error-message "the value of up-delay and down-delay "
            + "both can not be zero.";
            description
              "the value of up-delay and down-delay both can not
               be 0";
          }
          default "2500";
          description
            "Set link-down dampening delay";
        }
        leaf restart {
          type uint16 {
            range "0..65535";
          }
          default "0";
          description
            "Set link-up dampening delay on system restart";
        }
      }
    }
    /*
    auto-negotiate [ flc  {off | tx-only  | tx&rx | tx&rx-or-rx-
     only } | speed { 1000 | 100 | 10 | 10-or-100} | duplex {
     full | half  | full-or-half } | force { enable | disable} ]
    */
    container auto-negotiate {
      description
        "Set auto negotiation parameters";
      leaf auto-negotiate-enable {
        type boolean;
        default "true";
        description
          "Enable/Disable auto-negotiate-enable";
      }
      leaf flc {
        when "../auto-negotiate-enable='true'" {
          description
            "";
        }
        type enumeration {
          enum off {
            value 0;
            description
              "Set flow-control negotiated set to 'off'";
          }
          enum tx-only {
            value 1;
            description
              "Set flow-control negotiated set to 'Wish to
               Send'";
          }
          enum tx&rx {
            value 2;
            description
              "Set flow-control negotiated set to 'Wish to Send
               and Receive'";
          }
          enum tx&rx-or-rx-only {
            value 3;
            description
              "Set flow-control negotiated set to 'Wish to Send
               and Receive, or just Receive'";
          }
        }
        default "tx&rx-or-rx-only";
        description
          "Set flow-control negotiated set";
      }
      leaf speed {
        when "../auto-negotiate-enable='true'" {
          description
            "";
        }
        type enumeration {
          enum 1000 {
            value 0;
            description
              "Set port speed negotiated set to 1000 Mbps";
          }
          enum 100 {
            value 1;
            description
              "Set port speed negotiated set to 100 Mbps";
          }
          enum 10 {
            value 2;
            description
              "Set port speed negotiated set to 10 Mbps";
          }
          enum 10-or-100 {
            value 3;
            description
              "Set port speed negotiated set to 10 Mpbs or 100
               Mbps";
          }
        }
        description
          "Set port speed negotiated set";
      }
      leaf duplex {
        when "../auto-negotiate-enable='true'" {
          description
            "";
        }
        if-feature auto-negotiate-duplex;
        type enumeration {
          enum full {
            value 0;
            description
              "Set port duplex mode negotiated set to full
               duplex";
          }
          enum half {
            value 1;
            description
              "Set port duplex mode negotiated set to half
               duplex";
          }
          enum full-or-half {
            value 2;
            description
              "Set port duplex mode negotiated set to full
               duplex or half duplex";
          }
        }
        description
          "Set port duplex mode negotiated set";
      }
      leaf force {
        when "../auto-negotiate-enable='true'" {
          description
            "";
        }
        type enumeration {
          enum enable {
            value 0;
            description
              "
                              Force port UP if auto negotiation
               fails
                            ";
          }
          enum disable {
            value 1;
            description
              "
                              Set port to remain DOWN if auto
               negotiation fails
                            ";
          }
        }
        default "disable";
        description
          " Set port state if auto negotiation fails ";
      }
    }
    /*
    duplex { full | half }
    */
    leaf duplex {
      type enumeration {
        enum full {
          value 0;
          description
            "Set port duplex mode to full duplex";
        }
        enum half {
          value 1;
          description
            "Set port duplex mode to half duplex";
        }
      }
      description
        "Set port duplex mode";
    }
    /*
    speed { 1000 | 100  }
    */
    leaf speed {
      type enumeration {
        enum 1000 {
          value 0;
          description
            "Set port speed to 1000 Mbps";
        }
        enum 100 {
          value 1;
          description
            "Set port speed to 100 Mbps";
        }
      }
      description
        "Set port speed";
    }
    /*
    flow-control [rx|tx|tx&rx]
    */
    container flow-control {
      description
        "Enable/Disable flow control";
      leaf flow-control-enable {
        type boolean;
        default "true";
        description
          "Enable/Disable flow-control-enable";
      }
      leaf flow-control-opt {
        type enumeration {
          enum rx {
            value 0;
            description
              "Set flow-control for Receive only";
          }
          enum tx {
            value 1;
            description
              "Set flow-control for Send only";
          }
          enum tx&rx {
            value 2;
            description
              "Set flow-control for both Send and Receive";
          }
        }
        default "rx";
        description
          "leaf flow-control-opt.";
      }
    }
    /*
    loopback
    */
    leaf loopback {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      if-feature loopback;
      type empty;
      description
        "Enable loopback";
    }
    /*
    mirror { <policy (mirror-name) { in | out } [ source-tag {
     dot1q (tag-id) | dot1ad (tag-id)}  ]> | <destination (dest-
     name)> }
    */
    container mirror {
      description
        "Configure mirror policy or destination";
      choice mirror {
        description
          "IPOS choice";
        case policy {
          list policy {
            key "policy-choice";
            max-elements 2;
            description
              "Configure mirror policy";
            leaf policy-choice {
              type enumeration {
                enum in {
                  value 0;
                  description
                    "Configure inbound mirror policy";
                }
                enum out {
                  value 1;
                  description
                    "Configure outbound mirror policy";
                }
              }
              description
                "leaf policy-choice.";
            }
            leaf mirror-name {
              type leafref {
                path "/ctxsipos:contexts/mirrorpolicyipos"
                + ":mirror-policy/mirrorpolicyipos:mirror-polname";
              }
              mandatory true;
              description
                "Mirror policy name";
            }
            container source-tag {
              description
                "Configure mirror source tag";
              choice source-tag {
                description
                  "IPOS choice";
                case dot1q {
                  leaf dot1q {
                    type uint16 {
                      range "1..4094";
                    }
                    description
                      "Configure dot1q Ethernet type";
                  }
                }
                case dot1ad {
                  leaf dot1ad {
                    type uint16 {
                      range "1..4094";
                    }
                    description
                      "Configure dot1ad Ethernet type";
                  }
                }
              }
            }
          }
        }
        case destination {
          leaf destination {
            type string {
              length "1..39";
            }
            description
              "Configure mirror destination";
          }
        }
      }
    }
    /*
    igmp maximum-bandwidth (bandwidth-rate) [ percent ]
    */
    container igmp-maximum-bandwidth {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      presence "";
      description
        "Specify maximum bandwidth for a port";
      leaf bandwidth-rate {
        type uint16 {
          range "1..65535";
        }
        mandatory true;
        description
          "Rate in kbps";
      }
      leaf percent {
        type empty;
        description
          "Specfied rate is percentage of port bandwidth";
      }
    }
    /*
    ipv6 multicast maximum-bandwidth (bandwidth-rate) { kbps |
     mbps | gbps }
    */
    container ipv6-multicast-maximum-bandwidth {
      presence "";
      description
        "Specify maximum bandwidth for a port";
      leaf bandwidth-rate {
        type uint16 {
          range "1..65535";
        }
        mandatory true;
        description
          "assign bw in kbps | Mbps | Gbps";
      }
      choice ipv6-multicast-maximum-bandwidth-choice {
        description
          "IPOS choice";
        case kbps {
          leaf kbps {
            type empty;
            description
              "Kilo bits per sec";
          }
        }
        case mbps {
          leaf mbps {
            type empty;
            description
              "Mega bits per sec";
          }
        }
        case gbps {
          leaf gbps {
            type empty;
            description
              "Giga bits per sec";
          }
        }
      }
    }
    /*
    storm-control { pps | kbps }
    */
    container storm-control {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      description
        "Configure storm-control parameters";
      choice storm-control {
        description
          "IPOS choice";
        case pps {
          container pps {
            presence "";
            description
              "Packets-per-second";
            uses port-storm-ctrl-pps-grp;
          }
        }
        case kbps {
          container kbps {
            presence "";
            description
              "Kilobits-per-second";
            uses port-storm-ctrl-kbps-grp;
          }
        }
      }
    }
    /*
    port-type { wan-phy | 1ge | 10ge | 100ge }
    */
    leaf port-type {
      when "not(../../if:name = 'management')" {
        description
          "not applicable under management port";
      }
      type enumeration {
        enum wan-phy {
          value 0;
          description
            "Select WAN-PHY as port physical layer type";
        }
        enum 1ge {
          value 1;
          description
            "Select 1GE port type";
        }
        enum 10ge {
          value 2;
          description
            "Select 10GE port type";
        }
        enum 100ge {
          value 3;
          description
            "Select 100GE port type";
        }
      }
      description
        "The physical type of the port";
    }
  }

  grouping port-storm-ctrl-kbps-grp {
    description
      "storm control configuration parameters of kbps";
    /*
    unknown-dest { rate (rate-range) burst (size-range) }
    */
    container unknown-dest {
      presence "";
      description
        "Unknown destination frame storm-control parameters";
      leaf rate {
        type uint32 {
          range "122..1000000";
        }
        mandatory true;
        description
          "Rate limit in kbps";
      }
      leaf burst {
        type uint32 {
          range "10000..4000000";
        }
        mandatory true;
        description
          "Burst size limit in byte";
      }
    }
    /*
    multicast { rate (rate-range) burst (size-range) }
    */
    container multicast {
      presence "";
      description
        "Multicast frame storm-control parameters";
      leaf rate {
        type uint32 {
          range "122..1000000";
        }
        mandatory true;
        description
          "Rate limit in kbps";
      }
      leaf burst {
        type uint32 {
          range "10000..4000000";
        }
        mandatory true;
        description
          "Burst size limit in byte";
      }
    }
    /*
    broadcast { rate (rate-range) burst (size-range) }
    */
    container broadcast {
      presence "";
      description
        "Broadcast frame storm-control parameters";
      leaf rate {
        type uint32 {
          range "122..1000000";
        }
        mandatory true;
        description
          "Rate limit in kbps";
      }
      leaf burst {
        type uint32 {
          range "10000..4000000";
        }
        mandatory true;
        description
          "Burst size limit in byte";
      }
    }
  }

  grouping port-storm-ctrl-pps-grp {
    description
      "storm control configuration parameters of pps";
    /*
    unknown-dest-rate (rate-range)
    */
    leaf unknown-dest-rate {
      type uint32 {
        range "238..2000000";
      }
      description
        "Unknown destination frame storm-control parameter, rate
         limit in pps";
    }
    /*
    multicast-rate (rate-range)
    */
    leaf multicast-rate {
      type uint32 {
        range "238..2000000";
      }
      description
        "Multicast frame storm-control parameter, rate limit in
         pps";
    }
    /*
    broadcast-rate (rate-range)
    */
    leaf broadcast-rate {
      type uint32 {
        range "238..2000000";
      }
      description
        "Broadcast frame storm-control parameter, rate limit in
         pps";
    }
  }

  augment "/if:interfaces/if:interface" {
    description
      "ericsson-ethernet";
    /*
    ethernet
    */
    container ethernet {
      when "../if:type = 'ianaift:ethernetCsmacd'" {
        description
          "";
      }
      presence "";
      description
        "Select port to configure";
      uses port-grp;
    }
  }

}
