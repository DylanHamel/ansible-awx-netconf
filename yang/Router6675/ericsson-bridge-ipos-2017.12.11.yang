module ericsson-bridge-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-bridge-ipos";

  prefix "bridgeipos";

  import ietf-yang-types {
    prefix "yang";
  }

  import ietf-interfaces {
    prefix "if";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-bridge-ipos
     Copyright (c) 2017 Ericsson AB.
     All rights reserved";

  revision "2017-12-11" {
    description
      "IPOS-18766 For bridge yang model, change the type of si-
       id from uint32 to int32";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "2";
  }

  revision "2017-07-26" {
    description
      "IPOS-13767 Modify the range of si-id of bridge model";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "1";
  }

  revision "2017-06-30" {
    description
      "Change Mac Address type";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  revision "2017-06-17" {
    description
      "JIRA IPOS-11937 New command for bridge split-horizon-
       group under service-instance";
    reference
      "rfc6020";
  }

  revision "2017-05-21" {
    description
      "JIRA IPOS-10743 New command for model bridging";
    reference
      "rfc6020";
  }

  revision "2016-12-27" {
    description
      "Bridge instance and profile sequence change; remove
       unused import";
    reference
      "rfc6020";
  }

  revision "2016-03-29" {
    description
      "For CLI2Yang models data node sequence change.";
    reference
      "rfc6020";
  }

  revision "2015-12-07" {
    description
      "initial revision";
    reference
      "rfc6020";
  }

  feature restricted {
    description
      "This feature indicates that the device supports the
       restricted in bridge profile.";
  }

  grouping bridge-grp {
    description
      "Bridge configuration mode";
    /*
    learning
    */
    leaf learning {
      type boolean;
      default "true";
      description
        "Enable/Disable learning";
    }
    /*
    qualified-learning
    */
    leaf qualified-learning {
      type boolean;
      default "true";
      description
        "Enable/Disable qualified-learning";
    }
    /*
    split-horizon-group (shg-name)
    */
    list split-horizon-group {
      key "split-horizon-group";
      description
        "create split-horizon-group";
      leaf split-horizon-group {
        type string;
        description
          "split-horizon-group name";
      }
    }
    /*
    mac-drop
    */
    container mac-drop {
      description
        "drop mac-address";
      list mac {
        key "mac-address";
        description
          "mac-entry mac-addr";
        leaf mac-address {
          type yang:mac-address;
          description
            "mac-entry mac-addr";
        }
      }
      list vlan-mac {
        key "vlan mac";
        description
          "mac-entry vlan";
        leaf vlan {
          type uint16 {
            range "1..4094";
          }
          description
            "mac-entry vlan";
        }
        leaf mac {
          type yang:mac-address;
          description
            "mac-entry mac-addr";
        }
      }
    }
    /*
    mac-move-drop
    */
    leaf mac-move-drop {
      type boolean;
      default "true";
      description
        "Enable/Disable mac-move-drop";
    }
    /*
    aging-time (time-val)
    */
    leaf aging-time {
      type uint32 {
        range "60..655350";
      }
      description
        "set or reset aging timer in seconds";
    }
    /*
    description (desc-str)
    */
    leaf description {
      type string {
        length "1..63";
      }
      description
        "Add descriptive text for this bridge";
    }
    /*
    profile (prof-name)
    */
    leaf profile {
      type string {
        length "1..31";
      }
      description
        "Configure default bridge profile";
    }
    /*
    service-instance {interface(if-name)} {si-id(si-id-val)}
    */
    list service-instance {
      key "interface si-id";
      description
        "service-instance";
      leaf interface {
        type leafref {
          path "/if:interfaces/if:interface/if:name";
        }
        description
          "bridge attachment cct";
      }
      leaf si-id {
        type int32 {
          range "1..262143";
        }
        description
          "Service-instance identifier";
      }
      uses bridge-cct-grp;
    }
  }

  grouping bridge-profile-grp {
    description
      "Bridge profile configuration mode";
    /*
    mac-limit (limit-val)
    */
    leaf mac-limit {
      type uint32 {
        range "1..524288";
      }
      description
        "restrict the number of learned MAC addresses on a
         circuit";
    }
    /*
    restricted
    */
    leaf restricted {
      if-feature restricted;
      type empty;
      description
        "restrict the MACs on this profile, limit to statically
         configured MACs";
    }
    /*
    broadcast rate-limit (limit-value) burst (size-value)
    */
    container broadcast-rate-limit {
      presence "";
      description
        "rate-limit in kbps";
      leaf limit-value {
        type uint32 {
          range "5..1000000";
        }
        mandatory true;
        description
          "kbps";
      }
      leaf burst {
        type empty;
        mandatory true;
        description
          "Burst size";
      }
      leaf size-value {
        type uint32 {
          range "1..1250000000";
        }
        mandatory true;
        description
          "Burst size in bytes";
      }
    }
    /*
    multicast rate-limit (limit-value) burst (size-value)
    */
    container multicast-rate-limit {
      presence "";
      description
        "rate-limit in kbps";
      leaf limit-value {
        type uint32 {
          range "5..1000000";
        }
        mandatory true;
        description
          "kbps";
      }
      leaf burst {
        type empty;
        mandatory true;
        description
          "Burst size";
      }
      leaf size-value {
        type uint32 {
          range "1..1250000000";
        }
        mandatory true;
        description
          "Burst size in bytes";
      }
    }
    /*
    unknown-dest rate-limit (limit-value) burst (size-value)
    */
    container unknown-dest-rate-limit {
      presence "";
      description
        "rate-limit in kbps";
      leaf limit-value {
        type uint32 {
          range "5..1000000";
        }
        mandatory true;
        description
          "kbps";
      }
      leaf burst {
        type empty;
        mandatory true;
        description
          "Burst size";
      }
      leaf size-value {
        type uint32 {
          range "1..1250000000";
        }
        mandatory true;
        description
          "Burst size in bytes";
      }
    }
    /*
    description (desc-str)
    */
    leaf description {
      type string {
        length "1..63";
      }
      description
        "Add descriptive text for this profile";
    }
    /*
    flood unknown-unicast
    */
    leaf flood-unknown-unicast {
      type empty;
      description
        "Flood unknown unicast frames";
    }
  }

  grouping bridge-cct-grp {
    description
      "Bridge cct mode";
    /*
    profile (prof-name)
    */
    leaf profile {
      type string;
      description
        "Configure default bridge profile";
    }
    /*
    split-horizon-group (shg-name)
    */
    list split-horizon-group {
      key "split-horizon-group";
      description
        "Split Horizon Group of the service-instance";
      leaf split-horizon-group {
        type string;
        description
          "split-horizon-group name";
      }
    }
    /*
    static-mac-entry
    */
    container static-mac-entry {
      description
        "mac forwarding table entry";
      list mac {
        key "mac-address";
        description
          "MAC address list";
        leaf mac-address {
          type yang:mac-address;
          description
            "MAC address";
        }
      }
      list vlan-mac {
        key "vlan-id mac-address";
        description
          "MAC address list";
        leaf vlan-id {
          type uint16 {
            range "1..4094";
          }
          description
            "specify the vlan for the mac-entry";
        }
        leaf mac-address {
          type yang:mac-address;
          description
            "MAC address";
        }
      }
    }
  }

  container bridge-state {
    config "false";
    description
      "root model for get";
  }

  /*
  bridge
  */
  container bridge {
    description
      "Configure a bridge";
    list profile {
      key "profile";
      description
        "Configure a bridge profile";
      leaf profile {
        type string {
          length "1..31";
        }
        description
          "profile name";
      }
      uses bridge-profile-grp;
    }
    list bridge-instance {
      key "bridge-instance";
      description
        "Configure a bridge instance";
      leaf bridge-instance {
        type string {
          length "1..31";
        }
        description
          "bridge name";
      }
      uses bridge-grp;
    }
  }

}
