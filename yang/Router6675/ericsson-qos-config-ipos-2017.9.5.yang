module ericsson-qos-config-ipos {
  yang-version "1";

  namespace "urn:rdns:com:ericsson:oammodel:ericsson-qos-config-ipos";

  prefix "qoscfgipos";

  import ericsson-qos-ipos {
    prefix "qosipos";
  }

  import ericsson-contexts-ipos {
    prefix "ctxsipos";
  }

  import ericsson-context-ipos {
    prefix "ctxipos";
  }

  import ericsson-mpls-ipos {
    prefix "mplsipos";
  }

  import ericsson-yang-extensions {
    prefix "yexte";
  }

  organization
    "Ericsson AB";

  contact
    "Web:   <http://www.ericsson.com>";

  description
    "ericsson-qos-config-ipos
     Copyright (c) 2017 Ericsson AB.
     All rights reserved";

  revision "2017-09-05" {
    description
      "Add router mpls propagate qos from mpls mpls-enable";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "1";
  }

  revision "2017-06-22" {
    description
      "Add if-feature for ssr part";
    reference
      "rfc6020";
    yexte:version "1";
    yexte:release "0";
    yexte:correction "0";
  }

  revision "2017-05-02" {
    description
      "Fix jira IPOS-9312";
    reference
      "rfc6020";
  }

  revision "2017-03-15" {
    description
      "Add qos for ssr";
    reference
      "rfc6020";
  }

  revision "2016-03-29" {
    description
      "For CLI2Yang models data node sequence change.";
    reference
      "rfc6020";
  }

  revision "2016-02-29" {
    description
      "fix when/must condition issues";
    reference
      "rfc6020";
  }

  revision "2015-12-07" {
    description
      "initial revision";
    reference
      "rfc6020";
  }

  feature mpls-l2vpn {
    description
      "This feature indicates that the device supports the mpls-
       l2vpn in qos-config.";
  }

  feature mpls-lsr {
    description
      "This feature indicates that the device supports the mpls-
       lsr in qos-config.";
  }

  feature use-vlan-header {
    description
      "This feature indicates that the device supports the mpls-
       use-vlan-header in qos-config.";
  }

  feature use-vlan-ethertype {
    description
      "This feature indicates that the device supports the mpls-
       use-vlan-ethertype in qos-config.";
  }

  feature qos-conf-ssr {
    description
      "This feature indicates that the device supports ssr in
       qos .";
  }

  grouping flow-ac-profile-grp {
    description
      "Flow admission control profile configuration mode";
    /*
    max-flows-per-circuit (mfpc-num)
    */
    leaf max-flows-per-circuit {
      type uint32 {
        range "1..2097152";
      }
      description
        "Configure maximum flows per circuit";
    }
    /*
    sustained-creation-rate (scr-num)
    */
    leaf sustained-creation-rate {
      type uint32 {
        range "1..2097152";
      }
      description
        "Configure sustained flow creation rate per circuit";
    }
    /*
    burst-creation-rate (bcr-num)
    */
    leaf burst-creation-rate {
      type uint32 {
        range "1..2097152";
      }
      description
        "Configure burst flow creation rate per circuit";
    }
  }

  grouping circuit-group-grp {
    description
      "Circuit group configuration mode";
    /*
    qos { rate { maximum  (rate-num) | minimum  (rate-num)}}
    */
    container qos {
      description
        "Configure QoS parameters";
      container rate {
        description
          "Circuit rate limits";
        leaf maximum {
          type uint32 {
            range "64..1000000";
          }
          must "not (../minimum) or number(.) > "
          + "number(../minimum)" {
            error-message "min rate must be less than max rate";
            description
              "min rate must be less than max rate";
          }
          description
            "Maximum rate limit";
        }
        leaf minimum {
          when "not (../../weight)" {
            description
              "";
          }
          type uint32 {
            range "64..1000000";
          }
          must "not (../maximum) or number(.) < "
          + "number(../maximum)" {
            error-message "min rate must be less than max rate";
            description
              "min rate must be less than max rate";
          }
          description
            "Minimum rate limit";
        }
      }
      container policy {
        description
          "Configure policy";
        list metering {
          must "(./hierarchical or ./inherit)" {
            error-message "Must configure a pattern: "
            + "hierarchical or inherit";
            description
              "Must configure a pattern: hierarchical or
               inherit";
          }
          key "pol-name";
          max-elements 1;
          description
            "Set QoS metering policy to use";
          leaf pol-name {
            type leafref {
              path "/qosipos:qos/qosipos:policy/qosipos:gpol-"
              + "polname";
            }
            must "/qosipos:qos/qosipos:policy[qosipos:gpol-"
            + "polname=current()]/qosipos:metering" {
              error-message "policy type is not correct";
              description
                "Must specify a metering policy name";
            }
            description
              "Must specify a metering policy name";
          }
          choice metering-opt {
            description
              "IPOS choice";
            case inherit {
              leaf inherit {
                type empty;
                description
                  "inherit policy to children";
              }
            }
            case hierarchical {
              leaf hierarchical {
                type empty;
                description
                  "enable aggregate rate-limiting on the
                   children.note:hierarchical can't be set
                   currently!";
              }
            }
          }
          choice metering-opt1 {
            description
              "IPOS choice";
            case ip {
              container ip {
                presence "";
                description
                  "Configure IP attributes";
                leaf ipv6 {
                  type empty;
                  description
                    "Configure IPV6 attributes";
                }
                leaf acl-counters {
                  type empty;
                  mandatory true;
                  description
                    "Enable ACL counters";
                }
              }
            }
            case ipv6-acl-counters {
              leaf ipv6-acl-counters {
                type empty;
                description
                  "Enable ACL counters";
              }
            }
          }
        }
        list policing {
          must "(./hierarchical or ./inherit)" {
            error-message "Must configure a pattern: "
            + "hierarchical or inherit";
            description
              "Must configure a pattern: hierarchical or
               inherit";
          }
          key "pol-name";
          max-elements 1;
          description
            "Input policing";
          leaf pol-name {
            type leafref {
              path "/qosipos:qos/qosipos:policy/qosipos:gpol-"
              + "polname";
            }
            must "/qosipos:qos/qosipos:policy[qosipos:gpol-"
            + "polname=current()]/qosipos:policing" {
              error-message "policy type is not correct";
              description
                "Must specify a policing policy name";
            }
            description
              "Must specify a policing policy name";
          }
          choice policing-opt {
            description
              "IPOS choice";
            case inherit {
              leaf inherit {
                type empty;
                description
                  "inherit policy to children";
              }
            }
            case hierarchical {
              leaf hierarchical {
                type empty;
                description
                  "enable aggregate rate-limiting on the
                   children.note:hierarchical can't be set
                   currently!";
              }
            }
          }
          choice policing-opt1 {
            description
              "IPOS choice";
            case ip {
              container ip {
                presence "";
                description
                  "Configure IP attributes";
                leaf ipv6 {
                  type empty;
                  description
                    "Configure IPV6 attributes";
                }
                leaf acl-counters {
                  type empty;
                  mandatory true;
                  description
                    "Enable ACL counters";
                }
              }
            }
            case ipv6-acl-counters {
              leaf ipv6-acl-counters {
                type empty;
                description
                  "Enable ACL counters";
              }
            }
          }
        }
        leaf-list queuing {
          type leafref {
            path "/qosipos:qos/qosipos:policy/qosipos:gpol-"
            + "polname";
          }
          max-elements 1;
          description
            "Output queuing, must specify a";
        }
      }
      leaf hierarchical-mode-strict {
        type empty;
        description
          "Strict priority scheduling";
      }
      container profile {
        description
          "Configure profile";
        list overhead {
          key "name-overhead";
          max-elements 1;
          description
            "Overhead profile";
          leaf name-overhead {
            type leafref {
              path "/qosipos:qos/qosipos:profile/qosipos"
              + ":profile-name";
            }
            description
              "Overhead profile name";
          }
          leaf inherit {
            type empty;
            description
              "inherit profile to children";
          }
        }
        list resource {
          key "resource";
          max-elements 1;
          description
            "Resource profile";
          leaf resource {
            type leafref {
              path "/qosipos:qos/qosipos:profile/qosipos"
              + ":profile-name";
            }
            description
              "Resource profile name";
          }
        }
      }
      leaf weight {
        type uint16 {
          range "1..4096";
        }
        default "70";
        description
          "Node weight";
      }
    }
  }

  augment "/ctxsipos:contexts/ctxipos:context/ctxipos:router/mpl"
  + "sipos:mpls/mplsipos:propagate" {
    description
      "ericsson-qos-config";
    /*
    qos { from { mpls [ class-map-option [ class-map (map-name)
     ] [ l2vpn-class-map (map-name) ] ] | mpls-lsr [ class-map
     (map-name) ] } | to { mpls [ class-map-option [ class-map
     (map-name) ] [ l2vpn-class-map (map-name) ] ] | mpls-lsr [
     class-map (map-name) ] } | use-vlan-header { inner | outer }
     | use-vlan-ethertype { 8100 | 9100 | 9200 | 88a8 | (custom-
     hex) }  }
    */
    container qos {
      description
        "Propagate QoS classification values";
      container from {
        description
          "Set packet classification on ingress";
        container mpls {
          description
            "Set packet classification from MPLS EXP bits on
             ingress";
          leaf mpls-enable {
            type boolean;
            default "true";
            description
              "Enable/Disable mpls-enable";
          }
          container class-map-option {
            when "../mpls-enable='true'" {
              description
                "";
            }
            description
              "Specify class map option";
            leaf class-map {
              type string;
              must "/qosipos:qos/qosipos:class-map[qosipos:map-"
              + "name=current()]/qosipos:mpls/qosipos:in" {
                error-message "class-map type is not correct";
                description
                  "Ingress MPLS classification map name";
              }
              description
                "Use a custom packet classification mapping";
            }
            leaf l2vpn-class-map {
              type leafref {
                path "/qosipos:qos/qosipos:class-map/qosipos"
                + ":map-name";
              }
              description
                "Use a custom packet classification mapping for
                 l2vpn";
            }
          }
        }
        container mpls-lsr {
          description
            "lsr packet classification mapping";
          leaf mpls-lsr-enable {
            type boolean;
            default "true";
            description
              "Enable/Disable mpls-lsr-enable";
          }
          leaf class-map {
            when "../mpls-lsr-enable='true'" {
              description
                "";
            }
            type leafref {
              path "/qosipos:qos/qosipos:class-map/qosipos:map-"
              + "name";
            }
            description
              "Use a custom packet classification mapping for
               lsr";
          }
        }
      }
      container to {
        description
          "Set packet classification from internal packet
           classification on egress";
        container mpls {
          description
            "Set MPLS EXP bits from internal packet
             classification on egress";
          leaf mpls-enable {
            type boolean;
            default "true";
            description
              "Enable/Disable mpls-enable";
          }
          container class-map-option {
            when "../mpls-enable='true'" {
              description
                "";
            }
            description
              "Specify class map option";
            leaf class-map {
              type leafref {
                path "/qosipos:qos/qosipos:class-map/qosipos"
                + ":map-name";
              }
              description
                "Use a custom packet classification mapping";
            }
            leaf l2vpn-class-map {
              type leafref {
                path "/qosipos:qos/qosipos:class-map/qosipos"
                + ":map-name";
              }
              description
                "Use a custom packet classification mapping for
                 l2vpn";
            }
          }
        }
        container mpls-lsr {
          description
            "lsr packet classification mapping";
          leaf mpls-lsr-enable {
            type boolean;
            default "true";
            description
              "Enable/Disable mpls-lsr-enable";
          }
          leaf class-map {
            when "../mpls-lsr-enable='true'" {
              description
                "";
            }
            type leafref {
              path "/qosipos:qos/qosipos:class-map/qosipos:map-"
              + "name";
            }
            description
              "Use a custom packet classification mapping in
               lsr";
          }
        }
      }
      leaf use-vlan-header {
        type enumeration {
          enum inner {
            value 0;
            description
              "Use the 802.1p value from the CVLAN header";
          }
          enum outer {
            value 1;
            description
              "Use the 802.1p value from the SVLAN header";
          }
        }
        default "inner";
        description
          "Propagate from ethernet, use dot1p from given header";
      }
      leaf use-vlan-ethertype {
        type union {
          type enumeration {
            enum 8100 {
              value 0;
              description
                "8100 ether type (hexadecimal)";
            }
            enum 9100 {
              value 1;
              description
                "9100 ether type (hexadecimal)";
            }
            enum 9200 {
              value 2;
              description
                "9200 ether type (hexadecimal)";
            }
            enum 88a8 {
              value 3;
              description
                "88a8 ether type (hexadecimal)";
            }
          }
          type string {
            pattern '0x[0-9a-fA-F]{1,4}';
          }
        }
        default "8100";
        description
          "Propagate from ethernet, layer2 payload has given
           ethertype";
      }
    }
  }

  augment "/ctxsipos:contexts" {
    description
      "ericsson-qos-config";
    /*
    flow admission-control profile (prof-name)
    */
    container flow-admission-control-profile {
      if-feature qos-conf-ssr;
      presence "";
      description
        "Configure a Flow Admission-Control Profile";
      leaf prof-name {
        type string;
        mandatory true;
        description
          "Specify the name of a Flow Admission-Control profile";
      }
      uses flow-ac-profile-grp;
    }
    /*
    circuit-group (grp-name) { port (port-val) [ virtual-port ]
     | link-group (lg-name)[ virtual-port ] | parent-circuit-
     group (cg-name) }
    */
    list circuit-group {
      if-feature qos-conf-ssr;
      key "grp-name";
      description
        "Configure a circuit group";
      leaf grp-name {
        type string;
        description
          "Specify the name of the circuit-group";
      }
      choice circuit-group-choice {
        description
          "IPOS choice";
        case port {
          list port {
            key "port-val";
            description
              "Specify the port for this circuit-group";
            leaf port-val {
              type string;
              description
                "Enter slot/port";
            }
            leaf virtual-port {
              type empty;
              description
                "Specify virtual port model";
            }
            uses circuit-group-grp;
          }
        }
        case link-group {
          list link-group {
            key "lg-name";
            description
              "Specify the link-group for this circuit-group";
            leaf lg-name {
              type string;
              description
                "Specify the name of the link-group";
            }
            leaf virtual-port {
              type empty;
              description
                "Specify virtual port model";
            }
            uses circuit-group-grp;
          }
        }
        case parent-circuit-group {
          list parent-circuit-group {
            key "parent-circuit-group";
            description
              "Specify the parent-circuit-group for this
               circuit-group";
            leaf parent-circuit-group {
              type string;
              description
                "Specify the name of the parent-circuit-group";
            }
            uses circuit-group-grp;
          }
        }
      }
    }
  }

  augment "/ctxsipos:contexts/ctxipos:dot1q" {
    description
      "ericsson-qos-config";
    /*
    propagate qos {< to [ inner | both ] ethernet [ class-map
     (class-name) ] >| <from [ inner ] ethernet [ class-map
     (class-name) ]> }
    */
    container propagate-qos {
      must "./to or ./from" {
        error-message "must have sub's to or from.";
        description
          "must have sub's to or from.";
      }
      presence "";
      description
        "Propagate QoS classification values";
      container to {
        presence "";
        description
          "Set packet external priority from internal packet
           classification on egress";
        choice to-opt {
          description
            "IPOS choice";
          case inner {
            leaf inner {
              type empty;
              description
                "Set 802.1p bits of inner tag from internal
                 packet classification on egress";
            }
          }
          case both {
            leaf both {
              type empty;
              description
                "Set 802.1p bits of inner and outer tags from
                 internal packet classification on egress";
            }
          }
        }
        leaf ethernet {
          type empty;
          mandatory true;
          description
            "Set ethernet 802.1p bits from internal packet
             classification on egress";
        }
        leaf class-map {
          when "/qosipos:qos/qosipos:class-"
          + "map/qosipos:ethernet/qosipos:out" {
            description
              "";
          }
          type leafref {
            path "/qosipos:qos/qosipos:class-map/qosipos:map-"
            + "name";
          }
          must "/qosipos:qos/qosipos:class-map[qosipos:map-name "
          + "= current()]/qosipos:ethernet/qosipos:out" {
            error-message "class-map must be ethernet type for "
            + "out direction";
            description
              "leafref to class-map with ethernet out type";
          }
          description
            "Use a custom packet classification mapping";
        }
      }
      container from {
        presence "";
        description
          "Set internal packet classification from packet
           external priority on ingress";
        leaf inner {
          type empty;
          description
            "Set internal packet classification from 802.1p bits
             of inner tag on egress";
        }
        leaf ethernet {
          type empty;
          description
            "Set internal packet classification from ethernet
             802.1p bits on ingress";
        }
        leaf class-map {
          when "/qosipos:qos/qosipos:class-"
          + "map/qosipos:ethernet/qosipos:in" {
            description
              "";
          }
          type leafref {
            path "/qosipos:qos/qosipos:class-map/qosipos:map-"
            + "name";
          }
          must "/qosipos:qos/qosipos:class-map[qosipos:map-name "
          + "= current()]/qosipos:ethernet/qosipos:in" {
            error-message "class-map must be ethernet type for "
            + "in direction";
            description
              "leafref to class-map with ethernet in type";
          }
          description
            "Use a custom packet classification mapping";
        }
      }
    }
  }

}
