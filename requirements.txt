ncclient==0.6.7
ansible==2.9.6
molecule==3.0.2
jxmlease==1.0.1
pyang==2.2.1